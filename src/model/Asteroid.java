package model;

public class Asteroid {

    // Eigenschaften
    private int x;
    private int y;
    protected int h;
    protected int w;
    protected float speedY;
    private int type;

    // Konstruktoren
    public Asteroid(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Methoden
    public void update(long elapsedTime){
        this.y = Math.round(this.y + elapsedTime*speedY);
    }

    public boolean collision(Player player) {
//        System.out.println("Player: " + player.getX() + ", " + player.getY() + "Asteroid: " + this.getX() + ", " + this.getY());
        if (((player.getX() > (this.getX() - (player.getW()/2 + this.getW()/2)) &&
                (player.getX() < (this.getX() + (player.getW()/2 + this.getW()/2 )))))
            &&
            ((player.getY() > (this.getY() - (player.getH()/2 + this.getH()/2)) &&
                    (player.getY() < (this.getY() + (player.getH()/2 + this.getH()/2 )))))) {
            return true;
        }
        return false;
    }

    // Setter + Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}