package model;

public class Asteroid2 extends Asteroid {
    private int type = 1;

    public Asteroid2(int x, int y) {
        super(x, y);
        this.h = 35;
        this.w = 35;
        this.speedY = 0.2f;
        super.setType(2);
    }
}
