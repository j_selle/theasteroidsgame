package model;

public class Background {
    private double y;
    private float scrollSpeed;

    public Background() {
        this.y = 397*0.5;
        this.scrollSpeed = 0.05f;

    }

    public void update(long elapsedTime){
        this.y = (this.y >= 0 ? this.y - elapsedTime * scrollSpeed : 0.5 * 397);
    }


    public double getY() {
        return y;
    }
}
