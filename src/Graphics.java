import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Asteroid;
import model.Model;

import javax.swing.*;

public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;

    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    // Methoden
    public void draw() {

        // Clear Screen
        gc.clearRect(0,0, Model.WIDTH, Model.HEIGHT);

        // Draw Background
        Image space = new Image("file:Resources/space2.png");
        gc.drawImage(space, 0, model.getBackground().getY(), 384*0.5, 397*0.5,0, 0, Model.WIDTH, Model.HEIGHT);


        // Draw Asteroids
        drawAsteroids(model, gc);
        // Draw Player
        drawPlayer(model, gc);
        // Draw Lives
        drawLives(model, gc);
        // Draw Level #
        drawLevel(model, gc);
    }



    private static void drawAsteroids(Model model, GraphicsContext gc) {
        for (Asteroid asteroid : model.getAsteroids()) {

            Image asteroid1IMG = new Image("file:Resources/asteroid1.png");
            Image asteroid2IMG = new Image("file:Resources/asteroid2.png");
            Image asteroid3IMG = new Image("file:Resources/asteroid3.png");
            if (asteroid.getType() == 1) {
                gc.drawImage(asteroid1IMG, asteroid.getX() - asteroid.getW() / 2,
                        asteroid.getY() - asteroid.getH() / 2, asteroid.getW(), asteroid.getH());
            }
            else if (asteroid.getType() == 2) {
                gc.drawImage(asteroid2IMG, asteroid.getX() - asteroid.getW() / 2,
                        asteroid.getY() - asteroid.getH() / 2, asteroid.getW(), asteroid.getH());
            }
            else {
                gc.drawImage(asteroid3IMG, asteroid.getX() - asteroid.getW() / 2,
                        asteroid.getY() - asteroid.getH() / 2, asteroid.getW(), asteroid.getH());
            }
        }
    }

    private static void drawPlayer(Model model, GraphicsContext gc) {
        if (model.getPlayer().getLives() > 0) {
            Image spaceship1 = new Image("file:Resources/spaceship1.png");
            gc.drawImage(spaceship1, model.getPlayer().getX() - model.getPlayer().getW()/2,
                    model.getPlayer().getY() - model.getPlayer().getH()/2, model.getPlayer().getW(),
                    model.getPlayer().getH());
        }
        else {
            Image explosion = new Image("file:Resources/explosion.png");
            gc.drawImage(explosion, model.getPlayer().getX() - 2*model.getPlayer().getW(),
                    model.getPlayer().getY() - 2*model.getPlayer().getH(), 4*model.getPlayer().getW(),
                    4*model.getPlayer().getH());
            try {
                Thread.sleep(40);
                JOptionPane.showMessageDialog(null, "Game Over!");
            }
            catch (InterruptedException ex) {}

        }
    }

    private static void drawLives(Model model, GraphicsContext gc) {
        Image heart = new Image("file:Resources/heart.png");
        switch (model.getPlayer().getLives()){
            case (3): {
                gc.drawImage(heart, Model.WIDTH - 80, 20, 20, 20);
                gc.drawImage(heart, Model.WIDTH - 50, 20, 20, 20);
                gc.drawImage(heart, Model.WIDTH - 20, 20, 20, 20);
                break;
            }
            case (2): {
                gc.drawImage(heart, Model.WIDTH - 40, 20, 20, 20);
                gc.drawImage(heart, Model.WIDTH - 20, 20, 20, 20);
                break;
            }
            case (1): {
                gc.drawImage(heart, Model.WIDTH - 20, 20, 20, 20);
                break;
            }
            default: {}
        }
    }

    private static void drawLevel(Model model, GraphicsContext gc) {
        gc.setFill( Color.RED );
        gc.setStroke( Color.BLACK );
        gc.setLineWidth(2);
        Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 20 );
        gc.setFont( theFont );
        gc.fillText( "Level " + model.getLevel(), 30, 20 );
        gc.strokeText( "Level " + model.getLevel(), 30, 20 );
    }











}
